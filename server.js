const express = require('express');
const app = express();

// set the view engine to ejs
app.set('view engine', 'ejs')

app.use('/', express.static('./'))

// index
app.get('/', function (req, res) {
	res.render('pages/index')
})

app.get('/term-condition', function (req, res) {
	res.render('pages/term-condition')
})


app.listen(5000)
console.log('Company profile start at 5000')
